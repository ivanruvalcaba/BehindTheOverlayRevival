/**
 * File: background.js
 *
 * Copyright (c) 2017, 2018, 2019, Iván Ruvalcaba <ivanruvalcaba[at]protonmail[dot]com>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Get the translated string for context menu.
const contextMenuRemoveOverlayTitle = browser.i18n.getMessage(
    'contextMenuRemoveOverlayTitle');

function protocolIsApplicable(tabUrl) {
    const APPLICABLE_PROTOCOLS = ['http:', 'https:'];
    let url = new URL(tabUrl);
    return APPLICABLE_PROTOCOLS.includes(url.protocol);
}

// Initialize the page action. Only operates on tabs whose URL's protocol is applicable.
async function initializePageAction(tab) {
    if (protocolIsApplicable(tab.url)) {
        if (await userAlwaysWantsIcon() === true) {
            browser.pageAction.show(tab.id);
        } else {
            browser.pageAction.hide(tab.id);
        }

        if (await userAlwaysWantsContextMenu() === true) {
            // Create all the context menu items.
            browser.menus.create({
                id: 'separator',
                type: 'separator',
                contexts: ['page']
            });
            browser.menus.create({
                id: 'removeOverlay',
                type: 'normal',
                contexts: ['page'],
                title: contextMenuRemoveOverlayTitle
            });
        } else {
            browser.menus.remove('separator');
            browser.menus.remove('removeOverlay');
        }
    }
}

// Main script.
function removeOverlay() {
    // No tabs or host permissions needed!
    browser.tabs.executeScript(null, { file: '/js/overlay_remover.js' },
        function() {
            browser.tabs.executeScript(null, { code: 'overlayRemoverRun();' });
        });
}

// When first loaded, initialize the page action for all tabs.
browser.tabs.query({}).then((tabs) => {
    for (let tab of tabs) {
        initializePageAction(tab);
    }
});

// Each time a tab is updated, reset the page action for that tab.
browser.tabs.onUpdated.addListener((id, changeInfo, tab) => { // eslint-disable-line
    initializePageAction(tab);
});

// The click event listener, where we perform the appropriate action given the
// ID of the menu item that was clicked.
browser.menus.onClicked.addListener(function(info) {
    if (info.menuItemId === 'removeOverlay') {
        removeOverlay();
    }
});

// Shortcut listener.
browser.commands.onCommand.addListener(function(command) {
    if (command === 'removeOverlay') {
        removeOverlay();
    }
});

// Returns true if user set option to always display the page action.
async function userAlwaysWantsIcon() {
    let option = await browser.storage.local.get('alwaysShowPageAction');

    if (typeof option.alwaysShowPageAction !== 'boolean') {
        return false;
    } else {
        return option.alwaysShowPageAction;
    }
}

// Returns true if user set option to always display the context menu.
async function userAlwaysWantsContextMenu() {
    let option = await browser.storage.local.get('alwaysShowContextMenu');

    if (typeof option.alwaysShowContextMenu !== 'boolean') {
        return false;
    } else {
        return option.alwaysShowContextMenu;
    }
}

function showChangelog(details) {
    if (details.reason === 'install' || details.reason === 'update') {
        browser.tabs.create({ url: '../html/update-notes.html' });
    }
}

// Called when the user clicks on the page action.
browser.pageAction.onClicked.addListener(removeOverlay);
browser.browserAction.onClicked.addListener(removeOverlay);

// Called when the user install the extension or update to new release.
browser.runtime.onInstalled.addListener(showChangelog);
