/**
 * File: options.js
 *
 * Copyright (c) 2019, Iván Ruvalcaba <ivanruvalcaba[at]protonmail[dot]com>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const saveOptions = (e) => {
    browser.storage.local.set({
        alwaysShowPageAction: document.querySelector('#options-show-page-action').checked,
        alwaysShowContextMenu: document.querySelector('#options-show-context-menu').checked
    });
    e.preventDefault();
};

async function restoreOptions() {
    // Firefox 53 will erroneously complain that "ReferenceError: browser is not defined"
    let options = await browser.storage.local.get([
        'alwaysShowPageAction',
        'alwaysShowContextMenu'
    ]);

    if (typeof options.alwaysShowPageAction !== 'undefined') {
        document.querySelector('#options-show-page-action').checked = options.alwaysShowPageAction;
    }

    if (typeof options.alwaysShowContextMenu !== 'undefined') {
        document.querySelector('#options-show-context-menu').checked = options.alwaysShowContextMenu;
    }
}

document.addEventListener('DOMContentLoaded', restoreOptions);
document.querySelector('form').addEventListener('submit', saveOptions);
